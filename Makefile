################################################################
# Makefile for creating debian package for fm-utility-vaultsmart-storage-cleanuptool
#
# Run:
# $ make         -- creates the debian package.
# $ make deploy  -- creates the debian package and deploys it to $(REPO_HOST).
# $ make clean   -- removes all project related deb files.
################################################################

#########################################
# Package Metadata
#########################################
PROJECT_NAME=fm-service-akareader
VERSION=$(shell git describe --tags --always)
ARCH=amd64
CATEGORY=fusemail
VENDOR=fusemail
MAINTAINER=dev@fusemail.com
DESCRIPTION='akadb reader API service'
HOMEPAGE=https://bitbucket.org/fusemail/fm-service-akareader
TARGET=$(PROJECT_NAME)_$(VERSION)_$(ARCH).deb
GITHASH=$(shell git rev-parse HEAD)
BUILDDATE=$(shell date '+%Y-%m-%d_%H:%M:%S_%Z')
# WARNING: should use your own path
#SYSPKG=bitbucket.org/fusemail/fm-lib-commons-golang/sys
SYSPKG=bitbucket.org/fusemail/fm-service-akareader/vendor/bitbucket.org/fusemail/fm-lib-commons-golang/sys
INSTALLPATH='/usr/local/fusemail/fm-service-akareader'

#########################################
# Packaging Options
#########################################
FPM=/usr/local/bin/fpm
FILES=fm-service-akareader

#########################################
# Deployment Config 
#########################################
REPO_HOST=repo
REPO_PATH=/var/www/repo/amd64

.PHONY: build clean deploy help

$(TARGET): build build/fm-service-akareader
	# build debian package in build directory
	$(FPM) -n $(PROJECT_NAME)\
		   -p build/$(PROJECT_NAME)_$(VERSION)_$(ARCH).deb \
            --version=$(VERSION)\
            --force\
            -s dir\
            -t deb\
            --deb-no-default-config-files\
            --vendor=$(VENDOR)\
            --category=$(CATEGORY)\
            --description=$(DESCRIPTION)\
            --url=$(HOMEPAGE)\
            --maintainer=$(MAINTAINER)\
            --prefix=$(INSTALLPATH)\
	    build/fm-service-akareader=fm-service-akareader

docs/dist/api.html: docs/raml/*
	@mkdir -p docs/dist
	raml2html docs/raml/api.raml > docs/dist/api.html

bindata.go: README.md docs/dist/api.html
	go-bindata README.md docs/...

build: docs/dist/api.html bindata.go *.go
	@mkdir -p build
	go build -o build/fm-service-akareader -ldflags "\
		-X $(SYSPKG).Version=$(VERSION) \
		-X $(SYSPKG).GitHash=$(GITHASH) \
		-X $(SYSPKG).BuildStamp=$(BUILDDATE) \
	"
	cp build/fm-service-akareader build/fm-service-akareader-$(VERSION)

clean:
	echo $(TARGET)
	rm -rf build
	rm -f raml.html
	rm -f bindata.go

deploy: build
	@echo 'deploy disabled for now.' 
	#scp $< root@$(REPO_HOST):$(REPO_PATH)

help:
	@echo " $ make         -- creates the debian package."
	@echo " $ make deploy  -- creates the debian package and deploys it to $(REPO_HOST)."
	@echo " $ make clean   -- removes all project related deb files."
