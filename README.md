# Overview

This is a akareader service in GOLANG.

This service is built with FuseMail's Golang webservice common library. See basic setup instructions here: https://bitbucket.org/fusemail/fm-lib-commons-golang (README)

See documntation for all available endpoints to this service [here](/api).

See below for all system endpoints:

URLs | Desc
--- | ---
[/api](/api) | RAML documentation
[/health](/health) | Dependency health status
[/metrics](/metrics) | Service metrics
[/version](/version) | Version info
[/sys](/sys) | System info
[/log](/log) | Logs
[/trace](/trace) | Trace requests
[/debug/pprof/](/debug/pprof/) | Profiling

Available profiling endpoints are:

* [/debug/pprof/cmdline](/debug/pprof/cmdline)
* [/debug/pprof/profile](/debug/pprof/profile)
* [/debug/pprof/symbol](/debug/pprof/symbol)
* [/debug/pprof/trace](/debug/pprof/trace)
* [/debug/pprof/goroutine](/debug/pprof/goroutine)
* [/debug/pprof/heap](/debug/pprof/heap)
* [/debug/pprof/threadcreate](/debug/pprof/threadcreate)
* [/debug/pprof/block](/debug/pprof/block)
* [/debug/pprof/mutex](/debug/pprof/mutex)

# Documentation


# Dependencies

## DB Dependency

* DB Host: mysql.local (read-only aka db host)
* DB Name: tracksmart_config
* Read-only tables: shard_availability, shard_routing

# How to build
To make deployable built, first run the following command:

    make build


Note that the default value for Version in the code sets to "DO NOT USE IN PRODUCTION", so if you see -v option prints this value then it means this build is not made using the Makefile which should not be used for deployment to production environment.

# How to run

To run, use the following command:

    ENVIRONMENT=dev ./build/fm-service-akareader
or

    ./build/fm-service-akareader -e dev

("dev" is the default value so can be ignored. Change "dev" to "prod" if it is to run in production environment.)

Then open your browser to http://localhost:10022 (Note: 10022 is the default port) for the web server root "/" page. And the following two end points should automatically becomes available:
http://localhost:10022/health : shows API health information with dependency checks
http://localhost:10022/metrics : shows API metrics

To see all available command line options, run:

    ./build/fm-service-akareader --help

##How to debug
Turn on debug mode using '-d' option in the following command:

    ./build/fm-service-akareader -d

# How to test

##How to run unit tests
To run test, you need to switch to the current directory first:

    cd $GOPATH/src/bitbucket.org/fusemail/fm-server-akareader/

The following command should test all *_test.go files (including all sub-directories):

    go test ./...

The following command will start a local web page at http://localhost:9090 to show test result as soon as your files are changed:

    goconvey -port 9090

To check test coverage, go to the package directory that you want to test and use the following command:

    go get golang.org/x/tools/cmd/cover
    go test -coverprofile cover.out

e.g 

    ~ $ cd $GOPATH/src/bitbucket.org/fusemail/fm-service-akareader/
    db $ go test -coverprofile cover.out 
    ... 

