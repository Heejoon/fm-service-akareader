// Package main provides the foundation for your service.
package main

import (
	"net/http"
	"os"

	"bitbucket.org/fusemail/fm-lib-commons-golang/bindata"
	"bitbucket.org/fusemail/fm-lib-commons-golang/health"
	"bitbucket.org/fusemail/fm-lib-commons-golang/metrics"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server/handlers"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server/middleware"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server/profiling"
	"bitbucket.org/fusemail/fm-lib-commons-golang/sys"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

var options struct {
	System      sys.Options               `group:"Default System Options"`
	Application server.ApplicationOptions `group:"Default Application Server Options"`

	Port int `long:"port" env:"PORT" description:"application port" default:"8080"`
}

func mountDefaultEndpoints(router *mux.Router, common *negroni.Negroni) {
	router.Handle(options.Application.DocRoute,
		common.With(negroni.Wrap(handlers.DocsMarkdown(bindata.LoadFile, options.Application.DocFile))))
	router.Handle(options.Application.APIRoute,
		common.With(negroni.Wrap(handlers.DocsHTML(bindata.LoadFile, options.Application.APIFile))))
	router.Handle(options.Application.HealthRoute,
		common.With(negroni.Wrap(handlers.Health())))
	router.Handle(options.Application.MetricsRoute,
		common.With(negroni.Wrap(handlers.Metrics())))
	router.Handle(options.Application.VersionRoute,
		common.With(negroni.Wrap(handlers.Version())))
	router.Handle(options.Application.SysRoute,
		common.With(negroni.Wrap(handlers.Sys(options))))
	router.Handle(options.Application.LogRoute,
		common.With(negroni.Wrap(handlers.Log(options.Application.LogCount))))
	router.Handle(options.Application.TraceRoute,
		common.With(negroni.Wrap(handlers.Trace())))

	// Mount profiler.
	profiling.RegisterMux(router)
}

func setupAndServeHealthChecks(configDB *ConfigDB) {
	health.Register(
		&health.Dependency{
			Name: "tracksmart_config database",
			Desc: "Manages tracksmart_config health",
			Item: configDB.SQLDB,
		},
	)
	health.Serve()
}

// metrics vectors
var (
	MetricErrorCounter = metrics.NewMetric(&metrics.Vector{
		Type:   metrics.TypeCounter,
		Name:   "akareader_error_counter",
		Desc:   "akaread service error counts",
		Labels: []string{"action", "error"},
	})
	MetricSQLQueryCounter = metrics.NewMetric(&metrics.Vector{
		Type:   metrics.TypeCounter,
		Name:   "sql_query_counter",
		Desc:   "DB query counter",
		Labels: []string{"query"},
	})
	MetricSQLQueryTimer = metrics.NewMetric(&metrics.Vector{
		Type:   metrics.TypeHistogram,
		Name:   "sql_query_duration_ms",
		Desc:   "DB query duration in milliseconds",
		Labels: []string{"query"},
	})
)

func setupAndServeMetrics() {
	vectors := metrics.NewMetricVectors([]*metrics.Metric{
		MetricSQLQueryCounter,
		MetricSQLQueryTimer,
	})

	metrics.Register(vectors...)
	metrics.Serve()
}

func main() {
	// Override system logging used by base libraries.
	system := sys.NewLogger(log.StandardLogger())
	system.Level = log.WarnLevel

	// Setup system.
	sys.SetLogger(system)
	sys.SetupOptions(&options, &options.System)

	// Setup bindata
	bindata.Setup(Asset, AssetDir, AssetNames)

	// Setup middleware.
	middleware.SetLogger(system)
	common := middleware.Common()

	// Define router.
	router := mux.NewRouter()

	// Setup server.
	server.SetLogger(system)
	server.Setup(&server.Config{
		Port:    options.Port,
		UseSSL:  options.Application.SSL,
		SSLCert: options.Application.SSLCert,
		SSLKey:  options.Application.SSLKey,
		Router:  router,
	})

	// Automatic consul registration and de-registration
	if options.Application.ConsulRegistration {
		service := &server.Service{
			Name:             options.Application.ConsulName,
			RegistrationHost: options.Application.ConsulHost,
			Port:             options.Port,
		}
		service.MustRegister()
		log.Debug("registered to consul: ", service)
		defer service.Deregister() // nolint:errcheck
	}

	mountDefaultEndpoints(router, common)

	// Setup database singleton instance
	configDB := setupConfigDB()

	limiter := common.With(middleware.NewLimiter(options.Application.Limit))

	router.Handle("/v1/rest/tracksmart/shard",
		limiter.With(negroni.Wrap(HandlerLookupShard(configDB)))).Methods(http.MethodGet)
	router.Handle("/v1/rest/tracksmart/shard/{shard_id}",
		limiter.With(negroni.Wrap(HandlerLookupShard(configDB)))).Methods(http.MethodGet)
	router.Handle("/v1/rest/tracksmart/shard_in_archiving",
		limiter.With(negroni.Wrap(HandlerLookupShardArchiving(configDB)))).Methods(http.MethodGet)

	// Setup health with dependencies.
	health.SetLogger(system)
	setupAndServeHealthChecks(configDB)

	// Setup metrics.
	metrics.SetLogger(system)
	setupAndServeMetrics()

	// Serve and block.
	log.Info("start.")
	server.Serve()
	sys.BlockAndFunc(func(os.Signal) {
		server.ShutdownAll()
	})
	log.Info("done.")
}
