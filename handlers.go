package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"bitbucket.org/fusemail/fm-lib-commons-golang/metrics"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server"
	"bitbucket.org/fusemail/fm-lib-commons-golang/server/middleware"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

// handlerError records handlers' own errors.
type handlerError struct {
	action           string
	err              error
	metricErrCounter *metrics.Metric
	logger           *log.Entry
}

func (e *handlerError) withMetric(m *metrics.Metric) *handlerError {
	e.metricErrCounter = m
	return e
}

func (e *handlerError) withLogger(logger *log.Entry) *handlerError {
	e.logger = logger
	return e
}

func newHandlerError(action string, err error) *handlerError {
	return &handlerError{
		action: action,
		err:    err,
	}
}

// Error implements error interface
func (e *handlerError) Error() string {
	return e.err.Error()
}

const defaultStatusCode = http.StatusBadRequest

// nolint
var (
	ErrRequestParseFailed        = errors.New("failed to parse request data")
	ErrRequiredParameterNotFound = errors.New("required query parameters not found")
	ErrInvalidCusNum             = errors.New("invalid customer number")
	ErrInvalidShardID            = errors.New("invalid shard ID")
	ErrNotFound                  = errors.New("shard not found")

	errToStatusMap = map[error]int{
		ErrNotFound: http.StatusNotFound,
	}
)

func (e *handlerError) errToStatusCode(err error) int {
	if statusCode, ok := errToStatusMap[err]; ok {
		return statusCode
	}

	return defaultStatusCode
}

func (e *handlerError) writeJSONResponse(w http.ResponseWriter) {
	e.metricErrCounter.AddOne(e.action, e.Error())

	if e.logger != nil {
		e.logger.WithField("error", e.Error()).Errorf("%s", e.action)
	}

	statusCode := e.errToStatusCode(e.err)

	data := map[string]string{
		"error": fmt.Sprintf("%d - %s", statusCode, e.Error()),
	}

	server.WriteJSONWithStatus(w, data, statusCode)
}

func HandlerLookupShard(configDB *ConfigDB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := middleware.GetContextLogger(r.Context())

		var err error
		var shardID int
		vars := mux.Vars(r)

		// validate {shard_id} if exists
		if value, ok := vars["shard_id"]; ok {
			shardID, _ = strconv.Atoi(value)
			if shardID == 0 {
				err = ErrInvalidShardID
				logger.WithFields(log.Fields{"err": err, "shard_id": value}).Error("HandlerLookupShard")
				newHandlerError("handlerLookupShard", err).withMetric(MetricErrorCounter).withLogger(logger).writeJSONResponse(w)
				return
			}
		}

		shards, err := configDB.GetShard(shardID)
		if err != nil {
			logger.WithField("err", err).Error("HandlerLookupShard")
			newHandlerError("handlerLookupShard", err).withMetric(MetricErrorCounter).withLogger(logger).writeJSONResponse(w)
			return
		}

		logger.WithField("number_of_shard", len(shards)).Info("HandlerLookupShard")

		server.WriteJSONWithStatus(w, shards, http.StatusOK)
	})
}

func HandlerLookupShardArchiving(configDB *ConfigDB) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := middleware.GetContextLogger(r.Context())

		customers, err := configDB.GetCustomer()
		if err != nil {
			newHandlerError("handlerLookupShardArchiving", err).withMetric(MetricErrorCounter).withLogger(logger.WithField("err", err)).writeJSONResponse(w)
			return
		}

		logger.WithField("number_of_customers", len(customers)).Info("HandlerLookupShardArching")

		server.WriteJSONWithStatus(w, customers, http.StatusOK)

	})
}
