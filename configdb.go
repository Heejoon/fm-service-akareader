package main

import (
	"fmt"
	"os"
	"time"

	"database/sql"

	"bitbucket.org/fusemail/fm-lib-commons-golang/dbutil"
	"bitbucket.org/fusemail/fm-lib-commons-golang/deps"
	"bitbucket.org/fusemail/fm-lib-commons-golang/metrics"
	"bitbucket.org/fusemail/fm-lib-commons-golang/sys"

	log "github.com/Sirupsen/logrus"
	"github.com/go-sql-driver/mysql"
)

// ConfigDB represents config information in sphinx_config database
type ConfigDB struct {
	config *mysql.Config
	SQLDB  *deps.SQLDB

	db *sql.DB

	counterMetric *metrics.Metric
	timerMetric   *metrics.Metric
}

// ConfigDBOption is a function type that executes on ConfigDB instance.
type ConfigDBOption func(*ConfigDB)

// WithQueryCounterMetric enables SQL query counter metric reporting.
func WithQueryCounterMetric(m *metrics.Metric) ConfigDBOption {
	return func(c *ConfigDB) {
		c.counterMetric = m
	}
}

// WithQueryTimerMetric enables SQL query counter metric reporting.
func WithQueryTimerMetric(m *metrics.Metric) ConfigDBOption {
	return func(c *ConfigDB) {
		c.timerMetric = m
	}
}

func setupConfigDB() *ConfigDB {
	conf := &mysql.Config{
		Net:    "tcp",
		Addr:   os.Getenv("DB_ADDR"),
		DBName: os.Getenv("DB_NAME"),
		User:   os.Getenv("DB_USER"),
		Passwd: os.Getenv("DB_PASS"),
	}

	configDB, err := NewConfigDB(conf, WithQueryCounterMetric(MetricSQLQueryCounter), WithQueryTimerMetric(MetricSQLQueryTimer))
	if err != nil {
		log.WithField("err", err).Error("setupConfigDB failed")
		sys.Exit(1)
	}

	return configDB
}

// NewConfigDB sets up a ConfigDB instance with provided MySQL config
func NewConfigDB(mysqlConfig *mysql.Config, options ...ConfigDBOption) (c *ConfigDB, err error) {
	c = &ConfigDB{
		config: mysqlConfig,
	}

	c.SQLDB = deps.NewSQLDB(c.config)

	err = c.Connect()
	if err != nil {
		return c, fmt.Errorf("failed to connect to [%v]: %v", mysqlConfig, err)
	}

	for _, option := range options {
		option(c)
	}

	return c, nil
}

func (c *ConfigDB) Connect() error {
	err := c.SQLDB.Connect()
	if err != nil {
		return err
	}

	c.db = c.SQLDB.DB

	return nil
}

/*
JOB: delete expired indexes

STEP1) query tracksmart_config db

	SELECT a.shard_id, retention_days, start_time, end_time
	FROM shard_availability a LEFT JOIN shard_routing r USING (shard_id);

STEP2) call SphinxIndexManager API
/v1/rest/shard/{shard_id}/index?start_time=&cutoff_days=

	SELECT index_name
	FROM sphinx_config.index_ownership
	WHERE
		start_ts > @start_time AND
		end_ts < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL @cutoff_days)) AND
		shard_id = @shard_id;
*/

/*
JOB: delete indexes from removed/unsubscribed customers

STEP1) GET list of {cus_num, shard_id, start_ts, end_ts} for ValutSMART customer

	SELECT cus_num, a.shard_id, start_time, end_time
	FROM shard_availability a LEFT JOIN shard_routing r USING (shard_id)
	WHERE shard_type = 'archiving';


STEP2) call existing API (/v1/rest/customer/{cus_num}/indexes?host_type=&start_ts=&end_ts=)

	SELECT index_name
	FROM sphinx_config.index_ownership
	WHERE
		sa.shard_id = shard_id AND
		sr.start_time = start_ts AND
		sr.end_time = end_ts AND
		sr.cus_num = cus_num

*/

// tracksmart_config.shard_routing.start_time
// 'start_time' int(11) DEFAULT '0' COMMENT 'unix timestamp'
func ToInt64ForStartTime(val interface{}) int64 {
	str := fmt.Sprintf("%v", val)
	if str == "<nil>" {
		return 0
	}
	return dbutil.ToInt64(str)
}

// tracksmart_config.shard_routing.end_time
// 'end_time' int(11) DEFAULT NULL COMMENT 'Unix timestamp, null means current'
func ToInt64ForEndTime(val interface{}) int64 {
	str := fmt.Sprintf("%v", val)
	if str == "<nil>" {
		return time.Now().Unix()
	}
	return dbutil.ToInt64(str)
}

type TimeRange struct {
	StartTime int64
	EndTime   int64
}

// Shard struct represents a row in shard_availability table to be deleted
type Shard struct {
	ShardID       int64
	RetentionDays int64
	TimeRanges    []TimeRange
}

func (c *ConfigDB) buildShardQuery(shardID int) *dbutil.Query {

	// build query no shardID
	if shardID == 0 {
		return &dbutil.Query{
			Pattern:        "SELECT a.shard_id, retention_days, start_time, end_time FROM shard_availability a LEFT JOIN shard_routing r USING (shard_id)",
			CounterMetric:  c.counterMetric,
			DurationMetric: c.timerMetric,
			MetricLabels:   []string{"list shard_id from shard_availability table"},
		}
	}

	// build query with shardID
	return &dbutil.Query{
		Pattern:        "SELECT a.shard_id, retention_days, start_time, end_time FROM shard_availability a LEFT JOIN shard_routing r USING (shard_id) where a.shard_id = ?",
		Args:           []interface{}{shardID},
		CounterMetric:  c.counterMetric,
		DurationMetric: c.timerMetric,
		MetricLabels:   []string{"list shard_id/{shard_id} from shard_availability table"},
	}

}

func (c *ConfigDB) GetShard(shardID int) (shards []*Shard, err error) {
	query := c.buildShardQuery(shardID)
	rows, err := query.ReadRowsMap(c.db)
	if err != nil {
		log.Error(query, err)
		return
	}
	log.Info("GetShard ", shardID, " ", err, " rows:", rows)

	// create hashmap for sorting out query result
	shardMaps := make(map[int64]*Shard)

	for _, row := range rows {
		shardID := dbutil.ToInt64(row["shard_id"])
		retentionDays := dbutil.ToInt64(row["retention_days"])

		if _, exists := shardMaps[shardID]; !exists {
			shardMaps[shardID] = &Shard{
				ShardID:       shardID,
				RetentionDays: retentionDays,
			}
		}

		timeRange := TimeRange{
			StartTime: ToInt64ForStartTime(row["start_time"]),
			EndTime:   ToInt64ForEndTime(row["end_time"]),
		}
		shardMaps[shardID].TimeRanges = append(shardMaps[shardID].TimeRanges, timeRange)
	}

	// return shards
	for _, shard := range shardMaps {
		shards = append(shards, shard)
	}

	if len(shards) == 0 {
		err = ErrNotFound
	}

	return
}

// VaultSMART Customer struct that has been removed/unsubscribed
type Customer struct {
	CusNum        int64
	ShardID       int64
	RetentionDays int64
	TimeRanges    []TimeRange
}

func (c *ConfigDB) GetCustomer() (customers []*Customer, err error) {
	query := &dbutil.Query{
		Pattern:        "SELECT a.shard_id, cus_num, retention_days, start_time, end_time FROM shard_availability a LEFT JOIN shard_routing r USING (shard_id) WHERE shard_type = 'archiving'",
		CounterMetric:  c.counterMetric,
		DurationMetric: c.timerMetric,
		MetricLabels:   []string{"list customer from shard_availability table"},
	}
	rows, err := query.ReadRowsMap(c.db)
	if err != nil {
		log.Error(query, err)
		return
	}

	// shardID  is the key
	shardMaps := make(map[int64]*Customer)

	for _, row := range rows {

		cusNum := dbutil.ToInt64(row["cus_num"])
		shardID := dbutil.ToInt64(row["shard_id"])
		retentionDays := dbutil.ToInt64(row["retention_days"])

		if _, exists := shardMaps[shardID]; !exists {
			shardMaps[shardID] = &Customer{
				CusNum:        cusNum,
				ShardID:       shardID,
				RetentionDays: retentionDays,
			}
		}

		timeRange := TimeRange{
			StartTime: ToInt64ForStartTime(row["start_time"]),
			EndTime:   ToInt64ForEndTime(row["end_time"]),
		}
		shardMaps[shardID].TimeRanges = append(shardMaps[shardID].TimeRanges, timeRange)
	}

	// return customer map
	for _, customer := range shardMaps {
		customers = append(customers, customer)
	}

	if len(customers) == 0 {
		err = ErrNotFound
	}

	return
}
